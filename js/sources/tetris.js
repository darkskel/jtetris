// Type de bloc pouvant etre genere
var suitebloc = [
				{"nom":"bloc1", "forme":['','1','','','','']},
				{"nom":"bloc2", "forme":['1','1','1','','1','']},
				{"nom":"bloc3", "forme":['1','1','1','','','']},
				{"nom":"bloc4", "forme":['','1','1','','1','']},
				{"nom":"bloc5", "forme":['','1','','','1','']}
				];


// Selecteur recurant
var score = $('#scorePoints');
var afficheNbbloc = $('#nbbloc');
var droite =$('#droite');
var generateur0 = $('#case_0');
var generateur1 = $('#case_1');
var generateur2 = $('#case_2');
var generateur3 = $('#case_3');
var generateur4 = $('#case_4');

// Variable global a l'application
// Gere le score et la partie
var perdu = false;
var nbScore =0;
var nbbloc =0;
// Gere les touches
var bloquerTouche=false;
var enfoncer = 0;
// Gere la generation
var bloque = true;
var nouveau = true;
var id =-1;
var blocEnMouvement;
var type;

// Permet d'afficher le top score au demarage
$(function(){
	recupTopScore();
	
});

// Initialise la partie
droite.on('click','#jouer', function(){	
	$('#lanceJeu').hide();
	$('#divNom').show();
	$('.bloc').remove(); // vide le tableau
	perdu = false;
	nouveau =true;
	bloque =true;
	nbScore =0;
	nbbloc =0;
	score.html(nbScore);
	type = suitebloc[Math.floor(Math.random() * 5 )];	
	generebloc('','');
});
// Permet l'envoi de son score
droite.on('click','#valider', function(){	
	postScore($('#nom').val());
	recupTopScore();
	$('#divNom').hide();
});

/**
 * Permet la generation de bloc
 * @param id du bloc si existant n'importe quelle valeur nouveau == true
 * @param blocGenere instance de bloc n'importe quelle valeur nouveau == true
 */
function generebloc(id,blocGenere){	

	// Si fin partie
	if(perdu){
		$('#jouer').html('Perdu, Rejouer?');
		$('#divNom').html('Mon pseudo : <input type="texte" id="nom"/><input type="button" value="ok" id="valider"/>');
		$('#lanceJeu').show();	

	// Si un bloc tombe
	}else if(!bloque){				
		setTimeout(
			function(){ 
				blocGenere.tombe();
				generebloc(id,blocGenere);
				
			}, 
		1000);
	// si un bloc et a generer	
	}else if(nouveau){
		
		if(!generateur0.is(':empty') || !generateur1.is(':empty')
		|| !generateur2.is(':empty') || !generateur3.is(':empty')
		|| !generateur4.is(':empty')){
			perdu= true;
		}else{
			nbbloc++;
			afficheNbbloc.html(nbbloc);			
			bloque=false;			
			id++;
			blocEnMouvement = blocGenere = new bloc(id,type);
			type = suitebloc[Math.floor(Math.random() * 5 )];			
			blocGenere.create(Math.floor(Math.random() * 5 ));			
			nouveau =false;
			afficheSuivant();
		}
		generebloc(id,blocGenere);	
	// Si bloquer on prepare la generation d'un nouveau	
	}else{		
		setTimeout(
			function(){ 
				bloque=true;
				nouveau =true;
				verifieLigne();
				generebloc(id,blocGenere);
				
			}, 
		500);
	}
}

// Gere la fluidités des touches 
$(document).keyup(function(e){
	if (  e.which == 38 || e.which == 90 || e.which == 81 || e.which == 68 || e.which == 37 || e.which == 39 || e.which == 40 || e.which == 83) {
		enfoncer = 0;
	}
});
$(document).keydown(function(e) {
	touches(e.which);
});
// Gere les touche fleches et qzsd
function touches(touche){
	if(!bloquerTouche) {
		if (touche == 38) {
			bloquerTouche = true;
			enfoncer = 38;
			blocEnMouvement.bouge("haut");
		} else if (touche == 90) {
			bloquerTouche = true;
			enfoncer = 90;
			blocEnMouvement.bouge("haut");
		}else if (touche == 81) {
			bloquerTouche = true;
			enfoncer = 81;
			blocEnMouvement.bouge("gauche");
		} else if (touche == 68) {
			bloquerTouche = true;
			enfoncer = 68;
			blocEnMouvement.bouge("droite");
		}  else if (touche == 37) {
			bloquerTouche = true;
			enfoncer = 37;
			blocEnMouvement.bouge("gauche");
		} else if (touche == 39) {
			bloquerTouche = true;
			enfoncer = 39;
			blocEnMouvement.bouge("droite");
		}else if (touche == 83) {
			bloquer = true;
			enfoncer = 83;
			blocEnMouvement.bouge("bas");
		}else if (touche == 40) {
			bloquer = true;
			enfoncer = 40;
			blocEnMouvement.bouge("bas");
		}
		setTimeout(function(){
			bloquerTouche = false;
			if(enfoncer !=0){
				touches(touche);
			}
		}, 200)
	}
}

/**
 * Genere l'affichage de l'indicateur du bloc suivant
 */
function afficheSuivant(){
	$('#suivant').children().html('');
	for(var i=0;i<7;i++){		
		if(this.type.forme[i] == '1'){			
			switch(i) {
				case 0:
					$('#suivant_0').html('<div class="petit bloc_'+this.id+' '+this.type.nom+'"></div>');
					break;
				case 1:
					$('#suivant_1').html('<div class="petit bloc_'+this.id+' '+this.type.nom+'"></div>');
					break;
				case 2:
					$('#suivant_2').html('<div class="petit bloc_'+this.id+' '+this.type.nom+'"></div>');
					break;
				case 3:
					$('#suivant_3').html('<div class="petit bloc_'+this.id+' '+this.type.nom+'"></div>');
					break;
				case 4:
					$('#suivant_4').html('<div class="petit bloc_'+this.id+' '+this.type.nom+'"></div>');
					break;
				case 5:
					$('#suivant_5').html('<div class="petit bloc_'+this.id+' '+this.type.nom+'"></div>');
					break;				
			}			
		}		
	}	
}
/**
 * Verifie que la ligne n'est pas pleine sinon augmente le score en consequence
 */
function verifieLigne(){
	
	for(var i=30;i>0;i-=5){
		
		if(!$('#case_'+i).is(':empty') && !$('#case_'+(i+1)).is(':empty') 
			&& !$('#case_'+(i+2)).is(':empty') && !$('#case_'+(i+3)).is(':empty')
			&& !$('#case_'+(i+4)).is(':empty') ){
				
			$('#case_'+i).html('');
			$('#case_'+(i+1)).html('');
			$('#case_'+(i+2)).html('');
			$('#case_'+(i+3)).html('');
			$('#case_'+(i+4)).html('');
			
			// decale les blocs du dessus vers le bas
			for(var j=i;j>-1;j--){
				$('#case_'+j).children().appendTo($('#case_'+(j+5)));
			}
			i=i+5;
			nbScore+= 100;
			score.html(nbScore);			
		}
	}
	
}
/**
 * Envoie le score 
 * @param nom du joueur a envoyer au serveur
 */
function postScore(nom){
	$.post("php/recupScore.php", { nom: nom, score : nbScore, bloc: nbbloc});
}
/**
 * Recupere le top score par methode callBack
 */
function recupTopScore(){	
	
	$('#divScript').html('<script src="http://pikminisland.no-ip.biz/tetris/php/recupScore.php?callback=afficheTopScore"" type="text/javascript"></script>');
	$('#divScript').html('');
}

/**
 * Fonction qui affiche le top score
 * @param data score recu
 */
function afficheTopScore(data){
		var html ="";
		for(var i=0;i<data.length;i++){	
			html += '<div class="topScore">'+data[i]['nom']+' : '+data[i]['score']+' nb bloc : '+data[i]['bloc']+'</div>';	
		}
		$('#topScore').html(html);
}
/**
 * Genere un objet bloc
 * @param id du bloc
 * @param type du bloc a genere
 */
function bloc(id,type){
	this.id = id;
	this.type = type;	
}
/**
 * Fonction de bloc gerant la gestion des mouvements par le joueurs
 * @param direction vers la quel bouger le bloc
 */
bloc.prototype.bouge = function(direction){
	
	var mouvementBloque = false;
	
	if(direction =="haut"){
		
		var id = this.id;
		autreDiv = $('.bloc_'+ this.id);		
		
		autreDiv.each(function() {		
			
			var maCase = $( this ).parent().attr("id").replace('case_','');
			var caseSuivante = $('#case_'+(parseInt(maCase)+5));
			var caseHaut = $('#case_'+(parseInt(maCase)-5));
			var caseLateralGauche = $('#case_'+(parseInt(maCase)-1));
			var caseLateralDroite = $('#case_'+(parseInt(maCase)+1));
			
			// Verrifie que le mouvement peux avoir lieu sa toucher un bord
			if(!$( this ).hasClass('numero1')  && !(!$( this ).hasClass('numero1') 		
				&&(( caseLateralDroite.children().hasClass('numero1') && ($('#case_'+(parseInt(maCase)-4)).is(':empty') || $('#case_'+(parseInt(maCase)-4)).children().hasClass('bloc_'+ id) ))
				||( caseLateralGauche.children().hasClass('numero1')  && ($('#case_'+(parseInt(maCase)+4)).is(':empty') || $('#case_'+(parseInt(maCase)+4)).children().hasClass('bloc_'+ id) ))
				||( caseSuivante.children().hasClass('numero1')       && ($('#case_'+(parseInt(maCase)+6)).is(':empty') || $('#case_'+(parseInt(maCase)+6)).children().hasClass('bloc_'+ id) )
					&& (!(parseInt(maCase)+6==0 || parseInt(maCase)+6==5 || parseInt(maCase)+6==10 || parseInt(maCase)+6==15 
					        || parseInt(maCase)+6==20 || parseInt(maCase)+6==25|| parseInt(maCase)+6==30)) 
				)
				||( caseHaut.children().hasClass('numero1')           && ($('#case_'+(parseInt(maCase)-6)).is(':empty') || $('#case_'+(parseInt(maCase)-6)).children().hasClass('bloc_'+ id) )
					&& (!(parseInt(maCase)-6==-1 || parseInt(maCase-6)==4 || parseInt(maCase)-6==9 || parseInt(maCase)-6==14 
					        || parseInt(maCase)-6==19 || parseInt(maCase)-6==24 || parseInt(maCase)-6==29))
				))
			)){	
				// Si le moindre blocage on interdit le mouvement
				mouvementBloque = true;				
				return false;
			}		
		});
		// Si aucun blocage on applique le mouvement
		if(!mouvementBloque ){			
			autreDiv.each(function() {
				
				var maCase = $( this ).parent().attr("id").replace('case_','');
				var caseSuivante = $('#case_'+(parseInt(maCase)+5));
				var caseHaut = $('#case_'+(parseInt(maCase)-5));
				var caseLateralGauche = $('#case_'+(parseInt(maCase)-1));
				var caseLateralDroite = $('#case_'+(parseInt(maCase)+1));
				
				if(!$( this ).hasClass('numero1') && caseLateralDroite.children().hasClass('numero1')){
					
					$( this ).appendTo($('#case_'+(parseInt($( this ).parent().attr("id").replace('case_',''))-4)));
					
				}else if(!$( this ).hasClass('numero1') && caseLateralGauche.children().hasClass('numero1')){
					
					$( this ).appendTo($('#case_'+(parseInt($( this ).parent().attr("id").replace('case_',''))+4)));
					
				}else if(!$( this ).hasClass('numero1') && caseSuivante.children().hasClass('numero1') ){
					
					$( this ).appendTo($('#case_'+(parseInt($( this ).parent().attr("id").replace('case_',''))+6)));
					
				}else if(!$( this ).hasClass('numero1') && caseHaut.children().hasClass('numero1')){
					
					$( this ).appendTo($('#case_'+(parseInt($( this ).parent().attr("id").replace('case_',''))-6)));
				}				
			});
		// On rezet le blocage	
		}else{
			mouvementBloque = false;
		}
		
	// meme principe que "haut"	
	}else if(direction =="gauche" ){
		var id = this.id;
		autreDiv = $('.bloc_'+ this.id);		
		
		autreDiv.each(function() {		
			
			var maCase = $( this ).parent().attr("id").replace('case_','');
			var caseSuivante = $('#case_'+(parseInt(maCase)+4));
			var caseSuivantePlus = $('#case_'+(parseInt(maCase)+9));
			var caseLateral = $('#case_'+(parseInt(maCase)-1));
			
			if(!((caseLateral.is(':empty') || caseLateral.children('div').hasClass('bloc_'+ id)) 
				&& caseSuivante.length && (caseSuivante.is(':empty') ||
				(caseSuivante.children('div').hasClass('bloc_'+ id) && caseSuivantePlus.is(':empty')))
				&& !(parseInt(maCase)-1==-1 || parseInt(maCase)-1==4 
				|| parseInt(maCase)-1==9 || parseInt(maCase)-1==14 || parseInt(maCase)-1==19 
				|| parseInt(maCase)-1==24 || parseInt(maCase)-1==29))){		
			
				mouvementBloque = true;	
				return false;				
			}		
		});
		if(!mouvementBloque ){
			autreDiv.each(function() {
				$( this ).appendTo($('#case_'+(parseInt($( this ).parent().attr("id").replace('case_',''))-1)));
			});
		}else{
			mouvementBloque = false;
		}
	
	// meme principe que "haut"
	}else if(direction =="droite"){
		
		var id = this.id;
		autreDiv = $('.bloc_'+ this.id);			
		
		autreDiv.each(function() {		
			
			var maCase = $( this ).parent().attr("id").replace('case_','');
			var caseSuivante = $('#case_'+(parseInt(maCase)+6));
			var caseSuivantePlus = $('#case_'+(parseInt(maCase)+11));
			var caseLateral = $('#case_'+(parseInt(maCase)+1));
			
			if(!((caseLateral.is(':empty') || caseLateral.children('div').hasClass('bloc_'+ id)) 
				&& caseSuivante.length && (caseSuivante.is(':empty') ||
				(caseSuivante.children('div').hasClass('bloc_'+ id) && caseSuivantePlus.is(':empty')))
				&& !(parseInt(maCase)+1==0 || parseInt(maCase)+1==5 || parseInt(maCase)+1==10 
				|| parseInt(maCase)+1==15 || parseInt(maCase)+1==20 || parseInt(maCase)+1==25 || parseInt(maCase)+1==30) )){	
			
				mouvementBloque = true;	
				return false;				
			}		
		});
		if(!mouvementBloque ){
			autreDiv.each(function() {
				$( this ).appendTo($('#case_'+(parseInt($( this ).parent().attr("id").replace('case_',''))+1)));
			});
		}else{
			mouvementBloque = false;
		}

	// Utilise la fonction tombe de bloc	
	}else if(direction =="bas"){
		this.tombe();
	}	
}

/**
 * Fonction gerant la chute automatique ou manuel
 */
bloc.prototype.tombe = function(){

	var id = this.id;
	autreDiv = $('.bloc_'+ this.id);		
	
	autreDiv.each(function() {		
		
		var maCase = $( this ).parent().attr("id").replace('case_','');
		var caseSuivante = $('#case_'+(parseInt(maCase)+5));
		var caseSuivantePlus = $('#case_'+(parseInt(maCase)+10));
		var caseSuivantePlusPlus = $('#case_'+(parseInt(maCase)+15));
		
		// Si le bloc peut se deplacer vers le bas sinon on bloque
		if(!(caseSuivante.length && 
		    (caseSuivante.is(':empty') 
			||(caseSuivante.children('div').hasClass('bloc_'+ id) 
			&& (caseSuivantePlus.is(':empty') || (caseSuivantePlusPlus.is(':empty') 
			    && caseSuivantePlus.children('div').hasClass('bloc_'+ id) )) ))
		)){			
			bloque = true;
			return false;
		}		
	});
	// On deplace le bloc
	if(!bloque){
		autreDiv.each(function() {
		$( this ).appendTo($('#case_'+(parseInt($( this ).parent().attr("id").replace('case_',''))+5)));
		});
	}
	
}
/** 
 * Fonction qui genere l'affichage du bloc
 * @param centre du bloc a generer (la case de depart du bloc)
 */
bloc.prototype.create = function(centre){	

	// si le bloc risque de depasser on recentre le point de depart
	if(centre == 0 && this.type.forme[0] == '1'){
		centre++;
	}else if(centre == 4 && this.type.forme[2] == '1'){
		centre--;
	}

	// Generation du bloc en fonction du paterne de celui si
	for(var i=0;i<7;i++){
		
		if(this.type.forme[i] == '1'){
			
			switch(i) {
				case 0:
					$('#case_'+(centre-1)).html('<div class="bloc numero0 bloc_'+this.id+' '+this.type.nom+'"></div>');
					break;
				case 1:
					$('#case_'+centre).html('<div class="bloc numero1 bloc_'+this.id+' '+this.type.nom+'"></div>');
					break;
				case 2:
					$('#case_'+(centre+1)).html('<div class="bloc numero2 bloc_'+this.id+' '+this.type.nom+'"></div>');
					break;
				case 3:
					$('#case_'+(centre+4)).html('<div class="bloc numero3 bloc_'+this.id+' '+this.type.nom+'"></div>');
					break;
				case 4:
					$('#case_'+(centre+5)).html('<div class="bloc numero4 bloc_'+this.id+' '+this.type.nom+'"></div>');
					break;
				case 5:
					$('#case_'+(centre+6)).html('<div class="bloc numero5 bloc_'+this.id+' '+this.type.nom+'"></div>');
					break;
				
			}			
		}		
	}	
}