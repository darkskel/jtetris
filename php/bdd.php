<?php
/**
 * Class qui gere les acces bdd
 */
class Bdd{
	
	// Objet pdo	
	private $pdo;
	
	// info bdd	
	private $dsn ="mysql:dbname=;host=";
	private $user ="";
	private $password ="";
	
	/**	
     * Permet la creation automatique d'un objet pdo	
	 */		
	public function __construct(){	
		try{	
				$this->pdo = new PDO($this->dsn, $this->user, $this->password);	
			}catch(PDOException $e){	
				error_log($e->getMessage());	
			return 0;	
		}	
	}	
	
	/**	 
	 * Permet de faire des requetes de type "Select" 
	 * @param $requete sql de type prepare 
	 * @param $parametre array() associatif des parametre de la requete	 
	 * @return un array() associatif resultant de la requete	
	 */

	public function query($requete,$parametre){		
		try {		
			$requet = $this->pdo->prepare($requete);	
			$requet->execute($parametre);	
			$resultat = $requet->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {		
			error_log($e->getMessage());	
			return array();		
		}	
		return $resultat;
	}	
	
	/**	
     * Permet de faire des requetes de type "Update, delete, insert" 	
	 * @param $requete sql de type prepare 	
	 * @param $parametre array() associatif des parametre de la requete	 
	 * @return true si aucun probleme false sinon	
	 */

	public function exec($requete,$parametre){		
		try {		
			$requet = $this->pdo->prepare($requete);	
			$requet->execute($parametre);		
			$resultat = $requet->fetchAll(PDO::FETCH_ASSOC);	
		} catch (PDOException $e) {		
			error_log($e->getMessage());	
			return false;	
		}		
		return true;	
	}	

	/**
 	 * Detruit le pdo si l'objet bdd est supprimer	
	 */	
	 
	public function __destruct(){	
		unset($this->pdo);
	}
}
	