<?php 

require 'bdd.php';

$bdd = new Bdd();

// Si on post un score
if(!empty($_POST["nom"]) && !empty($_POST["score"]) && !empty($_POST["bloc"])){
	
	$sql   = "INSERT INTO score(nom,score,bloc) VALUES(:nom,:score,:bloc)";
	$param = array("nom"=>$_POST["nom"],"score"=>$_POST["score"],"bloc"=>$_POST["bloc"]);	
	$bdd->exec($sql,$param);

// Si on demande les scores	
}else {
	
	header('Content-Type: application/json');
	
	$sql   = "SELECT * FROM score ORDER BY score DESC LIMIT 0,15";
	$param = array();
	
	// SI fonction callback
	if(empty($_GET['callback'])){	
		echo json_encode($bdd->query($sql,$param),JSON_PRETTY_PRINT);		
	}else{
		echo $_GET['callback'] . '('.json_encode($bdd->query($sql,$param),JSON_PRETTY_PRINT).')';
	}
}
exit();